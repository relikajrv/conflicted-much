# Conflicted much?

A test repository to figure out whether we can ignore stuff in git with
attributes.

To try this out:

1. Clone & `git checkout branch-02`
1. make changes to files in the two folders
1. `echo a-my-conflicting-content >> allow-conflict/index.txt && echo b-my-conflicting-content >> ignore-conflict/index.txt`
1. Try a merge `git checkout branch-01 && git merge branch-02` (should get two conflicts)
1. Abort the merge `git merge --abort`
1. run `git config merge.ours.driver true` in the repo.
1. now try the same merge and you should only have one conflict.
